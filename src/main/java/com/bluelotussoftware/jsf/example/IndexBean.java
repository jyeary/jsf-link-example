/*
 *
 * Copyright 2013 Blue Lotus Software LLC. All Rights Reserved.
 *
 * $Id$
 *
 */
package com.bluelotussoftware.jsf.example;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@ManagedBean
@RequestScoped
public class IndexBean {

    public IndexBean() {
    }

    public String getRequestURL() {
        return "other";
    }
}